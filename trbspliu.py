# -*- coding: utf-8 -*-

"""
Subroutine to generate a rational B-spline curve using an uniform periodic knot vector

    C code for An Introduction to NURBS
    by David F. Rogers. Copyright (C) 2000 David F. Rogers,
    All rights reserved.
    
    Name: rbsplinu.c
    Language: C
    Subroutines called: knotu.c, rbasis.c, fmtmul.c
    Book reference: Chapter 4, Alg. p. 298

    b[]         = array containing the defining polygon vertices
                  b[1] contains the x-component of the vertex
                  b[2] contains the y-component of the vertex
                  b[3] contains the z-component of the vertex
    h[]         = array containing the homogeneous weighting factors 
    k           = order of the B-spline basis function
    nbasis      = array containing the basis functions for a single value of t
    nplusc      = number of knot values
    npts        = number of defining polygon vertices
    p[,]        = array containing the curve points
                  p[1] contains the x-component of the point
                  p[2] contains the y-component of the point
                  p[3] contains the z-component of the point
    p1          = number of points to be calculated on the curve
    t           = parameter value 0 <= t <= npts - k + 1
    x[]         = array containing the knot vector

    Name: trbspliu.c
    Purpose: Test periodic rational B-spline curve generator Chapter 4
    Language: C
    Subroutines called: rbspline.c
    Book reference:  Chapter 4, Sec. 4.2, Fig. 4.6, Alg. p 298
"""

import math as math
import numpy as np

class TrbSpliu:

    def __init__(self, npts, k):
        self.npts = npts
        self.k = k
        self.p1 = 105
        self.b = [None] * 31
        self.h = [None] * 30
        self.p = [None] * 601

        for i in range(1, 3*self.npts+1):
            self.b[i] = 0.0

        for i in range(1, self.npts+1):
            self.h[i] = 1.0

        self.h[3] = 1

        for i in range(1, 3*self.p1+1):
            self.p[i] = 0.0

        self.b[1] = 0
        self.b[2] = 0
        self.b[3] = 1
        self.b[4] = 1
        self.b[5] = 2
        self.b[6] = 1
        self.b[7] = 2.5
        self.b[8] = 0
        self.b[9] = 1
        self.b[10] = 4
        self.b[11] = 2
        self.b[12] = 1
        self.b[13] = 5
        self.b[14] = 0
        self.b[15] = 1

    def knotu(self, n, c, x):
        nplusc = n + c
        x[1] = 0
        for i in range(2, nplusc+1):
            x[i] = i-1

    def rbasis(self, c, t, npts, x, h, r):
        nplusc = npts + c
        temp = np.zeros(36)

        for i in range(1, nplusc):
            if t >= x[i] and t < x[i+1]:
                temp[i] = 1

        for k in range(2, c+1):
            for i in range(1, nplusc-k+1):
                if temp[i] != 0:
                    d = ((t-x[i])*temp[i])/(x[i+k-1]-x[i])
                else:
                    d = 0

                if temp[i+1] != 0:
                    e = ((x[i+k]-t)*temp[i+1])/(x[i+k]-x[i+1])
                else:
                    e = 0

                temp[i] = d + e

        if t == float(x[nplusc]):
            temp[npts] = 1

        sum = 0.0
        for i in range(1, npts+1):
            sum = sum + temp[i]*h[i]

        for i in range(1, npts+1):
            if sum != 0:
                r[i] = (temp[i]*h[i])/(sum)
            else:
                r[i] = 0

    def rbsplinu(self, npts, k, p1, b, h, p):
        nplusc = npts + k
        nbasis = [None] * 20
        x = [None] * 30

        for i in range(0, npts+1):
            nbasis[i] = 0.0

        for i in range(0, nplusc+1):
            x[i] = 0.0

        self.knotu(npts, k, x)
        icount = 0

        t = k-1
        step = (float((npts)-(k-1)))/(float(p1-1))
        for i1 in range(1, p1+1):
            if (float(x[nplusc]) - t) < 5*math.pow(10, -6):
                t = float(x[nplusc])

            self.rbasis(k, t, npts, x, h, nbasis)

            for j in range(1,4):
                jcount = j
                p[icount+j] = 0.0

                for i in range(1, npts+1):
                    temp = nbasis[i]*b[jcount]
                    p[icount + j] = p[icount + j] + temp
                    jcount = jcount + 3
            icount = icount + 3
            t = t + step

    def calculate(self):
        self.rbsplinu(self.npts, self.k, self.p1, self.b, self.h, self.p)

    def getCurvePoints(self):
        x = []
        y = []
        for i in range(1, (3*self.p1)+1, 3):
            x.append(float(self.p[i]))
            y.append(float(self.p[i+1]))

        return (x, y)

    def getPolygonPoints(self):
        x = []
        y = []
        for i in range(1, 3*self.npts+1, 3):
            x.append(float(self.b[i]))
            y.append(float(self.b[i+1]))

        return (x, y)

    def setPolygonPoints(self, b):
        self.b = b

    def getCurvePointsNum(self):
        return self.p1

    def setCurvePointNum(self, n):
        self.p1 = n

    def getWeights(self):
        return self.h

    def setWeights(self, weights):
        self.h = weights