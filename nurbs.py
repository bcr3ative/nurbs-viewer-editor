# -*- coding: utf-8 -*-

# Converting gui.ui to nurbsGui.py: pyside-uic -o nurbsGui.py gui.ui

from PySide.QtCore import *
from PySide.QtGui import *
import pyqtgraph as pg
import sys

import nurbsGui
import trbsplin as onurbs
import trbspliu as pnurbs
import dataManipulation as dm

# Class that handles GUI interaction
class MainDialog(QDialog, nurbsGui.Ui_mainDialog):

    def __init__(self, parent=None):
        super(MainDialog, self).__init__(parent)
        self.setupUi(self)

        self.obj = None
        self.curve = None
        self.polygon = None

        self.curveAtt = None
        self.polygonVertNum = None
        self.curveOrder = None
        # Semaphore variable
        self.cpInMotion = False

        self.plotArea.showGrid(x=1, y=1)

        # Connect a signal to the "Load template" button
        self.loadCurve.clicked.connect(self.loadTemplate)

        self.cpImport.clicked.connect(self.importControlPoints)
        self.cpExport.clicked.connect(self.exportControlPoints)
        self.curveExport.clicked.connect(self.exportCurvePoints)

    # Method merges two arrays into one grouping respective x & y coordinates ( [[x1, y1], [x2, y2]],... ).
    def len2seq(self, x, y):
        seq = []
        for i in range(len(x)):
            len2 = []
            len2.append(x[i])
            len2.append(y[i])
            seq.append(len2)

        return seq

    def importControlPoints(self):
        path = QFileDialog.getOpenFileName(self, 'Open control polygon', filter='*.nbp')
        if path[0] != '':
            importer = dm.Import(path[0])
            curveAtt, polygonVertNum, curveOrder, cpX, cpY, weights = importer.openPolygon()
            weights.insert(0, None)
            self.curveSel.setCurrentIndex(curveAtt)
            self.polygonVertNumSel.setValue(polygonVertNum)
            self.curveOrderSel.setValue(curveOrder)
            self.loadTemplate()
            self.updateCPCoordinates(cpX, cpY, self.polygonVertNum)
            self.updateCPWeights(weights, self.polygonVertNum)


    def exportControlPoints(self):
        path = QFileDialog.getSaveFileName(self, 'Save control polygon', filter='*.nbp')
        if path[0] != '':
            xp, yp = self.obj.getPolygonPoints()
            weights = self.obj.getWeights()
            exporter = dm.Export(path[0], curveAtt=self.curveAtt, polygonVertNum=self.polygonVertNum, curveOrder=self.curveOrder - 1, cpX=xp, cpY=yp, weights=weights)
            exporter.savePolygon()

    def exportCurvePoints(self):
        path = QFileDialog.getSaveFileName(self, 'Save curve', filter='*.nbc')
        if path[0] != '':
            x, y = self.obj.getCurvePoints()
            exporter = dm.Export(path[0], curveX=x, curveY=y)
            exporter.saveCurve()

    def enableCPSpinboxes(self, cpNum):
        cp = [self.cp1XSel, self.cp1YSel, self.cp1WeiSel,
              self.cp2XSel, self.cp2YSel, self.cp2WeiSel,
              self.cp3XSel, self.cp3YSel, self.cp3WeiSel,
              self.cp4XSel, self.cp4YSel, self.cp4WeiSel,
              self.cp5XSel, self.cp5YSel, self.cp5WeiSel]

        for i in range(0, 3*cpNum):
            cp[i].setEnabled(True)

        for i in range(3*cpNum, len(cp)):
            cp[i].setEnabled(False)

    def updateCPCoordinates(self, x, y, cpNum):
        cpX = [self.cp1XSel, self.cp2XSel, self.cp3XSel, self.cp4XSel, self.cp5XSel]
        cpY = [self.cp1YSel, self.cp2YSel, self.cp3YSel, self.cp4YSel, self.cp5YSel]

        for i in range(cpNum):
            cpX[i].setValue(x[i])
            cpY[i].setValue(y[i])

    def updateCPWeights(self, weights, cpNum):
        cpWeights = [self.cp1WeiSel, self.cp2WeiSel, self.cp3WeiSel, self.cp4WeiSel, self.cp5WeiSel]

        for i in range(cpNum):
            cpWeights[i].setValue(weights[i+1])

    # Method updates the curve plot on graph with new values.
    def updateCurvePlot(self, x, y):
        self.plotArea.removeItem(self.curve)
        self.curve = pg.PlotDataItem(x=x, y=y)
        self.plotArea.addItem(self.curve)

    def updatePolygonPlot(self, x, y):
        self.plotArea.removeItem(self.polygon)
        self.polygon = pg.PolyLineROI(self.len2seq(x, y), pen=(6, 9), closed=False, movable=False)
        self.plotArea.addItem(self.polygon)

        self.polygon.sigRegionChanged.connect(self.polygonMoved)

    def polygonChanged(self):
        if self.cpInMotion == False:
            cpX = [self.cp1XSel, self.cp2XSel, self.cp3XSel, self.cp4XSel, self.cp5XSel]
            cpY = [self.cp1YSel, self.cp2YSel, self.cp3YSel, self.cp4YSel, self.cp5YSel]

            # b array contains x, y, z coordinates of all control polygon points. Merges two arrays into one suitable
            # for usage in calculate() method in imported classes.
            b = []
            b.append(None)
            for i in range(self.polygonVertNum):
                b.append(cpX[i].value())
                b.append(cpY[i].value())
                b.append(1)

            # Calculate new curve points based on given control polygon points.
            self.obj.setPolygonPoints(b)
            self.obj.calculate()
            xp, yp = self.obj.getPolygonPoints()
            x, y = self.obj.getCurvePoints()

            self.updatePolygonPlot(xp, yp)
            self.updateCurvePlot(x, y)

    def weightsChanged(self):
        cpWeights = [self.cp1WeiSel, self.cp2WeiSel, self.cp3WeiSel, self.cp4WeiSel, self.cp5WeiSel]

        weights = []
        weights.append(None)
        for i in range(self.polygonVertNum):
            weights.append(cpWeights[i].value())

        self.obj.setWeights(weights)
        self.obj.calculate()
        x, y = self.obj.getCurvePoints()

        self.updateCurvePlot(x, y)

    # Method is called every time a control polygon point is moved. It handles curve plot updates.
    def polygonMoved(self):
        self.cpInMotion = True

        # x & y coordinates of all control polygon points.
        xp = []
        yp = []
        for i in range(len(self.polygon.getLocalHandlePositions())):
            xp.append(self.polygon.getLocalHandlePositions()[i][1].x())
            yp.append(self.polygon.getLocalHandlePositions()[i][1].y())

        self.updateCPCoordinates(xp, yp, self.polygonVertNum)

        # b array contains x, y, z coordinates of all control polygon points. Merges two arrays into one suitable
        # for usage in calculate() method in imported classes.
        b = []
        b.append(None)
        for i in range(len(xp)):
            b.append(xp[i])
            b.append(yp[i])
            b.append(1)

        # Calculate new curve points based on given control polygon points.
        self.obj.setPolygonPoints(b)
        self.obj.calculate()
        x, y = self.obj.getCurvePoints()

        self.updateCurvePlot(x, y)

        self.cpInMotion = False

    def curveVertNumChanged(self, n):
        self.obj.setCurvePointNum(n)
        self.obj.calculate()
        x, y = self.obj.getCurvePoints()
        self.updateCurvePlot(x, y)

    # Method is called every time a new template needs to be loaded into the graph.
    def loadTemplate(self):

        # Clear existing curves in the graph.
        self.plotArea.clear()

        # Open (0) or periodic (1).
        self.curveAtt = self.curveSel.currentIndex()
        # Number of control polygon points.
        self.polygonVertNum = self.polygonVertNumSel.value()
        # The order of the curve.
        self.curveOrder = self.curveOrderSel.value() + 1

        # Open
        if self.curveAtt == 0:
            # Instantiate TrbSplin class with given attributes.
            self.obj = onurbs.TrbSplin(self.polygonVertNum, self.curveOrder)

        # Periodic
        elif self.curveAtt == 1:
            # Instantiate TrbSpliu class with given attributes.
            self.obj = pnurbs.TrbSpliu(self.polygonVertNum, self.curveOrder)

        # Calculate curve points.
        self.obj.calculate()

        # Retrieve curve points from the template and plot the curve.
        x, y = self.obj.getCurvePoints()
        self.curve = pg.PlotDataItem(x=x, y=y)
        self.plotArea.addItem(self.curve)

        # Retrieve control polygon points from the template and plot the control polygon points.
        xp, yp = self.obj.getPolygonPoints()
        self.polygon = pg.PolyLineROI(self.len2seq(xp, yp), pen=(6, 9), closed=False, movable=False)
        self.plotArea.addItem(self.polygon)

        self.curveVertNumSel.setEnabled(True)
        self.curveVertNumSel.setValue(self.obj.p1)

        self.curveVertNumSel.sliderMoved.connect(self.curveVertNumChanged)

        self.enableCPSpinboxes(self.polygonVertNum)
        self.updateCPCoordinates(xp, yp, self.polygonVertNum)
        self.updateCPWeights(self.obj.getWeights(), self.polygonVertNum)

        self.cp1XSel.valueChanged.connect(self.polygonChanged)
        self.cp2XSel.valueChanged.connect(self.polygonChanged)
        self.cp3XSel.valueChanged.connect(self.polygonChanged)
        self.cp4XSel.valueChanged.connect(self.polygonChanged)
        self.cp5XSel.valueChanged.connect(self.polygonChanged)
        self.cp1YSel.valueChanged.connect(self.polygonChanged)
        self.cp2YSel.valueChanged.connect(self.polygonChanged)
        self.cp3YSel.valueChanged.connect(self.polygonChanged)
        self.cp4YSel.valueChanged.connect(self.polygonChanged)
        self.cp5YSel.valueChanged.connect(self.polygonChanged)

        self.cp1WeiSel.valueChanged.connect(self.weightsChanged)
        self.cp2WeiSel.valueChanged.connect(self.weightsChanged)
        self.cp3WeiSel.valueChanged.connect(self.weightsChanged)
        self.cp4WeiSel.valueChanged.connect(self.weightsChanged)
        self.cp5WeiSel.valueChanged.connect(self.weightsChanged)

        self.cpExport.setEnabled(True)
        self.curveExport.setEnabled(True)

        # Register a listener for changes on the polygon points.
        self.polygon.sigRegionChanged.connect(self.polygonMoved)

app = QApplication(sys.argv)
form = MainDialog()
form.show()
app.exec_()