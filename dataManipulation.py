# -*- coding: utf-8 -*-

class Import:

    def __init__(self, path):
        self.path = path

    def openPolygon(self):
        with open(self.path, "r") as f:
            content = f.read().splitlines()

        curveAtt = int(content[0])
        polygonVertNum = int(content[1])
        curveOrder = int(content[2])
        cpX = map(float, content[3].split())
        cpY = map(float, content[4].split())
        weights = map(float, content[5].split())

        return (curveAtt,polygonVertNum, curveOrder, cpX, cpY, weights)

class Export:

    def __init__(self, path, curveAtt=None, polygonVertNum=None, curveOrder=None, cpX=None, cpY=None, weights=None, curveX=None, curveY=None):
        self.path = path
        self.curveAtt = curveAtt
        self.polygonVertNum = polygonVertNum
        self.curveOrder = curveOrder
        self.cpX = cpX
        self.cpY = cpY
        self.weights = weights
        self.curveX = curveX
        self.curveY = curveY

    def saveCurve(self):
        with open(self.path, 'w') as f:
            for i in self.curveX:
                f.write(str(i) + ' ')
            f.write('\n')
            for i in self.curveY:
                f.write(str(i) + ' ')

    def savePolygon(self):
        with open(self.path, 'w') as f:
            f.write(str(self.curveAtt) + '\n')
            f.write(str(self.polygonVertNum) + '\n')
            f.write(str(self.curveOrder) + '\n')
            for i in self.cpX:
                f.write(str(i) + ' ')
            f.write('\n')
            for i in self.cpY:
                f.write(str(i) + ' ')
            f.write('\n')
            for i in range(1, self.polygonVertNum + 1):
                f.write(str(self.weights[i]) + ' ')